(ns piratas-do-devops.events
    (:require [re-frame.core :as re-frame]
              [piratas-do-devops.db :as db]))

(re-frame/reg-event-db
 :initialize-db
 (fn  [_ _]
   db/default-db))

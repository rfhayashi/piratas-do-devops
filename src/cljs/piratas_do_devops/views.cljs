(ns piratas-do-devops.views
    (:require [re-frame.core :as re-frame]))

(defn main-panel []
  (let [name (re-frame/subscribe [:name])]
    (fn []
        [:div
         [:div "Hello from " @name]
         [:div "Rui Fernando Hayashi"]
         [:div "Piratas do Devops"]])))

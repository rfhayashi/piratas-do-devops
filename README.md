# piratas-do-devops

A [re-frame](https://github.com/Day8/re-frame) application designed to ... well, that part is up to you.

## Development Mode

You need to have vagrant and virtualbox installed. You also need to install
the following vagrant plugins: vagrant-cachier, vagrant-fsnotify and nugrant.
 
After doing that, just run:

    vagrant up
    
When the virtual machine is running, run:

    vagrant ssh
    
and connect to the vm and change to /vagrant directory.

### Run application:

```
rake dev
```

Figwheel will automatically push cljs changes to the browser.

Wait a bit, then browse to [http://localhost:3449](http://localhost:3449).

## Production Build


To compile clojurescript to javascript:

```
rake build
```
